#include <iostream>		
#include <cmath>

class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		std::cout << '\n' << x << ' ' << y << ' ' << z << '\n';
		std::cout << sqrt (pow(x, 2) + pow(y, 2) + pow(z, 2));
	}
private:
	double x;
	double y;
	double z;
};

int main()
{
	Vector q(7,8,9);
	q.Show();
}